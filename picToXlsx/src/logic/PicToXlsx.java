package logic;

import java.util.List;

/**
 * Excel像素画生成器.
 * @author Rex
 *
 */
public class PicToXlsx {
	
	public static void main(String[] args) throws Exception {
		// 参数检查
		if (args.length != 2) {
			throw new Exception("参数数量有误");
		}
		if (!args[1].toLowerCase().endsWith(".xlsx")) {
			throw new Exception("输出路径必须是xlsx文件");
		}
		
		// list的容量为图片高度（行数），rgb数组的长度为图片宽度（列数）
		List<int[]> load = LoadPic.load(args[0]);
		// 填入到Excel
		CreateXlsx.create(load, args[1]);
	}
	
}